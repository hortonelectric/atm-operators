import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import * as Operator from './app/Content/Operator/reducer/reducer'
import * as OperatorModals from './app/Content/Operator/reducer/modalReducer'
import * as OperatorErrors from './app/Content/Operator/reducer/errorReducer'
import * as OperatorLoader from './app/Content/Operator/reducer/loaderReducer'

import * as Atm from './app/Content/ATM/reducer/reducer'
import * as AtmModals from './app/Content/ATM/reducer/modalReducer'
import * as AtmErrors from './app/Content/ATM/reducer/errorReducer'
import * as AtmLoader from './app/Content/ATM/reducer/loaderReducer'

import * as User from './app/User/reducer/reducer'
import * as UserError from './app/User/reducer/errorReducer'
import * as UserLoader from './app/User/reducer/loaderReducer'

import { alert } from './app/Alert/reducer'

const store = combineReducers({

	//Operator Store
    operator : combineReducers({
        list   		: Operator.operators,
        selected    : Operator.selected,
        modal       : combineReducers({
            add                : OperatorModals.operatorAddModal,
            editUser  		   : OperatorModals.operatorEditUserModal,
            editAccount        : OperatorModals.operatorEditAccountModal,
            delete             : OperatorModals.operatorDeleteModal
        }),
        error       : combineReducers({
            add             : OperatorErrors.addOperatorError,
            editUser       	: OperatorErrors.editOperatorUserError,
            editAccount     : OperatorErrors.editOperatorAccountError,
        }),
		loader		: combineReducers({
			table			: OperatorLoader.tableLoader,
            add             : OperatorLoader.addFormLoader,
            editUser  		: OperatorLoader.editUserFormLoader,
            editAccount     : OperatorLoader.editAccountFormLoader,
            delete          : OperatorLoader.deleteFormLoader
		})
    }),
	
	//Atm Store
    atm : combineReducers({
        list   		: Atm.atms,
        selected    : Atm.selected,
        modal       : combineReducers({
            add             : AtmModals.atmAddModal,
            edit  		   	: AtmModals.atmEditModal,
            delete          : AtmModals.atmDeleteModal
        }),
        error       : combineReducers({
            add         : AtmErrors.addAtmError,
            edit       	: AtmErrors.editAtmError,
        }),
		loader		: combineReducers({
			table			: AtmLoader.tableLoader,
            add             : AtmLoader.addFormLoader,
            edit  		   	: AtmLoader.editFormLoader,
            delete          : AtmLoader.deleteFormLoader
		})
    }),
	
	//User Store
	user        : combineReducers({
		profile : User.user,
		error   : UserError.error,
		loader	: UserLoader.loader
	}),
	
    // Alert Store
	alert,
	
    //redux-form
    form: formReducer,
})

export default store
