import React, { Component } from 'react'

import Header from './Header/Header'
import Menu from './Menu/Menu'
import Content from './Content/Content'
import Alert from './Alert/Alert'

class Container extends Component {
    render() {
		return (
				<div>
					<Header />	
					<Menu />
					<Content children={this.props.children}></Content>
					<Alert />
				</div>
		   )
	}
}

export default Container
