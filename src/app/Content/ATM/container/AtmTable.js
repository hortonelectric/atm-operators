import React, { Component } from 'react'
import { connect } from 'react-redux'
import { selectAtm } from '../action/action'

import  Table from '../component/Table'

class AtmTable extends Component {

    handleClick = (id) => {
        this.props.dispatch(selectAtm(id))
    }

    render() {
		const role = sessionStorage.getItem('role') 	

		const checkOperator = operator => { 
			if(operator.id && this.props.operators) {
				const selectedOperator = this.props.operators.find( item => item._id == operator.id )
				return selectedOperator.company
			}else{
				return operator.name
			} 
		}
		
		const rows = this.props.atms.map ((atm, index) => {

			if (role == 'admin') {
				return (
						<tr
							className={`employee-row-hover ${this.props.profile && this.props.profile._id  == atm._id ? 'employee-row-active' : ''}`}
							key={index}
							onClick={this.handleClick.bind(null, atm._id)}
						>
							<td>{atm.atm.type}</td>
							<td>{checkOperator(atm.operator)}</td>
							<td>{atm.location.location}</td>
						</tr>
				)
			}

			if (role == 'account'&& atm.operator.id == this.props.user._id) {
				return (
						<tr
							className={`employee-row-hover ${this.props.profile && this.props.profile._id  == atm._id ? 'employee-row-active' : ''}`}
							key={index}
							onClick={this.handleClick.bind(null, atm._id)}
						>
							<td>{atm.atm.type}</td>
							<td>{checkOperator(atm.operator)}</td>
							<td>{atm.location.location}</td>
						</tr>
				)
			}
		})

        return <Table rows={rows} loader={this.props.loader}/>
    }
}

export default connect( state => ({ 
	atms 		: state.atm.list,
	operators 	: state.operator.list[0] ? state.operator.list : null,
	user		: state.user.profile,
	loader		: state.atm.loader.table,
	profile 	: state.atm.selected ? 
					state.atm.list.find(atm => atm._id == state.atm.selected) : 
					null	
}))(AtmTable)
