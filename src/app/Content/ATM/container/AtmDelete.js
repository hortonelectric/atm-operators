import React, {Component} from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import { connect } from 'react-redux'

import { deleteAtm } from '../middleware/middleware'
import { hideAtmDeleteModal } from '../action/modalAction'

class AtmDelete extends Component {

    handleClick = () => {
		if(!this.props.loader) {
			this.props.dispatch(deleteAtm(this.props.profile))
		}
    }

    handleHide = () => {
        this.props.dispatch(hideAtmDeleteModal())
    }

	SubmitButton () {
		if(this.props.loader){
			return	(
				<button type="button" className="btn btn-danger btn-xl" disabled>
					<i className="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading
				</button>
			)
		}else{
			return (
				<button type="button" className="btn btn-danger btn-xl" onClick={this.handleClick}>
					<span>DELETE</span>
				</button>
		   )	
		}
	}

    render() {
		const type = this.props.profile ? this.props.profile.atm.type : null 
		const location = this.props.profile ? this.props.profile.location.location : null 
		const city = this.props.profile ? this.props.profile.location.address.city : null 

        return (
            <Modal show={this.props.modal} onHide={this.handleHide}>
                <div className="modal-body text-center p50">
                    <h1>{`Are you sure you wanted to delete ${type} in ${location} from ${city} city?`}</h1>
                </div>
                <div className="modal-footer text-center">
                    <button type="button" className="btn btn-default btn-xl" onClick={this.handleHide}>Close</button>
					{ this.SubmitButton() }
                </div>
            </Modal>
        )
    }
}

export default connect(
		state => ({
			modal: state.atm.modal.delete,
			loader: state.atm.loader.delete,
			profile : state.atm.selected ? 
				state.atm.list.find(atm => atm._id == state.atm.selected) : 
				null

		})	
	)(AtmDelete)
