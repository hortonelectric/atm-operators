import React,{Component} from 'react'
import {connect} from 'react-redux'

import * as Modal from '../action/modalAction'
import Profile_Blank from '../component/Profile_Blank'
import Profile from '../component/Profile'

class AtmDetail extends Component {

    handleAddAtm = () => {
        this.props.dispatch(Modal.showAtmAddModal())
    }

    handleDeleteAtm = () => {
        this.props.dispatch(Modal.showAtmDeleteModal())
    }

    handleEditAtm = () => {
        this.props.dispatch(Modal.showAtmEditModal())
    }

    render() {
        if(!this.props.profile) return <Profile_Blank handleAddAtm={this.handleAddAtm}/>
		if(this.props.profile) {
			return (
				<Profile 
					operators={this.props.operators}
					handleAddAtm={this.handleAddAtm}
					handleEditAtm={this.handleEditAtm}
					handleDeleteAtm={this.handleDeleteAtm}
				   	profile={this.props.profile}
			    />	
			)

		}	
    }
}

export default connect(
			state => ({
				operators: state.operator.list,
				profile : state.atm.selected ? 
					state.atm.list.find(atm => atm._id == state.atm.selected) : 
					null	
			})
		)(AtmDetail)
