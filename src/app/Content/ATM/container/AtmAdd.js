import React, {Component} from 'react'
import { reduxForm } from 'redux-form'

import { hideAtmAddModal } from '../action/modalAction'
import { addAtm } from '../middleware/middleware'

import AddForm from '../component/AddForm'

class AtmAdd extends Component {

    handleHide = () => {
        this.props.dispatch(hideAtmAddModal())
    }

    handleSubmitForm = (e) => {
        e.preventDefault()
		if(!this.props.loader){
			this.props.dispatch(addAtm(formatFields(this.props.values)))
		}
    }

    render () {
		
		const operatorOptions = this.props.operators.map( operator => {
			return <option value={operator._id} key={operator._id}>{ operator.company }</option>
		})

        return <AddForm handleHide={this.handleHide}
                        handleSubmitForm={this.handleSubmitForm}
						operatorOptions={operatorOptions}
                        {...this.props}
        />
    }
}

const formatFields = (data) => {
    return {
        atm : {
            type 		: data.type,
			supported	: data.supported,
			direction	: parseInt(data.direction),
            fees 		: data.fees,
            limits 		: data.limits,
            details 	: data.details
        },
        location : {
			lat			: data.lat,
			long 		: data.long,
            location	: data.location,
            address     : {
				street		: data.street,
				city		: data.city,
				state		: data.state,
				zip			: data.zip,
				country		: data.country
			},
			phone		: data.phone,
			openHours	: data.openHours
        },
		operator : {
			id 			: data.operator	
		}
    }
}

export default reduxForm({
    form: 'approverAdd',
    fields: [
        'type',
        'supported',
        'direction',
		'fees',
        'limits',
        'details',
        'lat',
        'long',
		'location',
        'street',
        'city',
        'state',
		'zip',
		'country',
		'phone',
		'openHours',
		'operator'
    ]}, 
	state => ({
		modal : state.atm.modal.add,
		ajaxErrors: state.atm.error.add,
		loader: state.atm.loader.add,
		operators: state.operator.list,
		initialValues: {
			operator : state.operator.list[0] ? checkUserCredentials(state) : null,
			supported : ["Bitcoin"],
			direction : "1",
			country	  : "United States"
		}

	})
)(AtmAdd)

const checkUserCredentials = (state) => {
	const role = sessionStorage.getItem('role') 	
	if(role == 'admin') return state.operator.list[0]._id
	if(role == 'account') return state.operator.list.find( operator => operator._id == state.user.profile._id )._id
}
