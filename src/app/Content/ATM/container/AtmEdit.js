import React, {Component} from 'react'
import { reduxForm } from 'redux-form'

import { hideAtmEditModal } from '../action/modalAction'
import { editAtm } from '../middleware/middleware'

import EditForm from '../component/EditForm'

class AtmEdit extends Component {

    handleHide = () => {
        this.props.dispatch(hideAtmEditModal())
    }

    handleSubmitForm = (e) => {
        e.preventDefault()
		if(!this.props.loader){
			this.props.dispatch(editAtm(this.props.atm, formatFields(this.props.values)))
		}
    }

    render () {
		
		const operatorOptions = this.props.operators.map( operator => {
			return <option value={operator._id} key={operator._id}>{ operator.company }</option>
		})

        return <EditForm handleHide={this.handleHide}
                        handleSubmitForm={this.handleSubmitForm}
						operatorOptions={operatorOptions}
                        {...this.props}
        />
    }
}

const formatFields = (data) => {
    return {
        atm : {
            type 		: data.type,
			supported	: data.supported,
			direction	: parseInt(data.direction),
            fees 		: data.fees,
            limits 		: data.limits,
            details 	: data.details
        },
        location : {
			lat			: data.lat,
			long 		: data.long,
            location	: data.location,
            address     : {
				street		: data.street,
				city		: data.city,
				state		: data.state,
				zip			: data.zip,
				country		: data.country
			},
			phone		: data.phone,
			openHours	: data.openHours
        },
		operator : {
			id 			: data.operator	
		}
    }
}

export default reduxForm({
    form: 'atmEdit',
    fields: [
        'type',
        'supported',
        'direction',
		'fees',
        'limits',
        'details',
        'lat',
        'long',
		'location',
        'street',
        'city',
        'state',
		'zip',
		'country',
		'phone',
		'openHours',
		'operator'
    ]}, 
	state => ({
		modal : state.atm.modal.edit,
		ajaxErrors: state.atm.error.edit,
		loader: state.atm.loader.edit,
		operators: state.operator.list,
		atm : state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected) : null,
		initialValues: {
			type 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.type: null, 
			supported 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.supported: null,
			direction 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.direction: null,
			fees 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.fees: null,
			limits 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.limits: null,
			details 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).atm.details: null,
			lat 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.lat: null,
			long 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.long: null,
			location 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.location: null,
			street 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.address.street: null,
			city 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.address.city: null,
			state 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.address.state: null,
			zip 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.address.zip: null,
			country 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.address.country: null,
			phone 		: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.phone: null,
			openHours 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).location.openHours: null,
			operator 	: state.atm.selected ? state.atm.list.find(item => item._id == state.atm.selected).operator.id: null
		}

	})
)(AtmEdit)
