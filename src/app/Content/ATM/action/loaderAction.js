export const ATM_TABLE_LOADER_ON 		= 'ATM_TABLE_LOADER_ON'
export const ATM_TABLE_LOADER_OFF 		= 'ATM_TABLE_LOADER_OFF'
export const ATM_ADD_FORM_LOADER_ON 	= 'ATM_ADD_FORM_LOADER_ON'
export const ATM_ADD_FORM_LOADER_OFF 	= 'ATM_ADD_FORM_LOADER_OFF'
export const ATM_EDIT_FORM_LOADER_ON 	= 'ATM_EDIT_FORM_LOADER_ON'
export const ATM_EDIT_FORM_LOADER_OFF 	= 'ATM_EDIT_FORM_LOADER_OFF'
export const ATM_DELETE_FORM_LOADER_ON 	= 'ATM_DELETE_FORM_LOADER_ON'
export const ATM_DELETE_FORM_LOADER_OFF	= 'ATM_DELETE_FORM_LOADER_OFF'


export const tableLoaderOn = () => {
    return {
        type: ATM_TABLE_LOADER_ON
    }
}

export const tableLoaderOff = () => {
    return {
        type: ATM_TABLE_LOADER_OFF
    }
}

export const addFormLoaderOn = () => {
    return {
        type: ATM_ADD_FORM_LOADER_ON
    }
}

export const addFormLoaderOff = () => {
    return {
        type: ATM_ADD_FORM_LOADER_OFF
    }
}

export const editFormLoaderOn = () => {
    return {
        type: ATM_EDIT_FORM_LOADER_ON
    }
}

export const editFormLoaderOff = () => {
    return {
        type: ATM_EDIT_FORM_LOADER_OFF
    }
}

export const deleteFormLoaderOn = () => {
    return {
        type: ATM_DELETE_FORM_LOADER_ON
    }
}

export const deleteFormLoaderOff = () => {
    return {
        type: ATM_DELETE_FORM_LOADER_OFF
    }
}
