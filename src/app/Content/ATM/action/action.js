export const LIST_ATMS                     = 'LIST_ATMS'
export const SELECT_ATM                    = 'SELECT_ATM'
export const ADD_ATM                       = 'ADD_ATM'
export const DELETE_ATM                    = 'DELETE_ATM'
export const EDIT_ATM 			            = 'EDIT_ATM'
export const UPLOAD_ATM_PICTURE            = 'UPLOAD_ATM_PICTURE'


export function listAtms(data) {
    return {
        type: LIST_ATMS,
        data
    }
}

export function selectAtm(id) {
    return {
        type: SELECT_ATM,
        id
    }
}

export const addAtm = (data) => {
    return {
        type: ADD_ATM,
        data
    }
}

export const deleteAtm = (data) => {
    return {
        type: DELETE_ATM,
        data
    }
}

export const editAtm = (data) => {
    return {
        type: EDIT_ATM,
        data
    }
}

export const uploadPicture = (data) => {
    return {
        type: UPLOAD_ATM_PICTURE,
        data
    }
}
