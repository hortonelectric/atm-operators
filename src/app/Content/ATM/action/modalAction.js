export const SHOW_ATM_ADD_MODAL         = 'SHOW_ATM_ADD_MODAL'
export const HIDE_ATM_ADD_MODAL         = 'HIDE_ATM_ADD_MODAL'
export const SHOW_ATM_DELETE_MODAL      = 'SHOW_ATM_DELETE_MODAL'
export const HIDE_ATM_DELETE_MODAL      = 'HIDE_ATM_DELETE_MODAL'
export const SHOW_ATM_EDIT_MODAL		= 'SHOW_ATM_EDIT_MODAL'
export const HIDE_ATM_EDIT_MODAL		= 'HIDE_ATM_EDIT_MODAL'

export function showAtmAddModal() {
    return {
        type: SHOW_ATM_ADD_MODAL
    }
}

export function hideAtmAddModal() {
    return {
        type: HIDE_ATM_ADD_MODAL
    }
}

export function showAtmDeleteModal() {
    return {
        type: SHOW_ATM_DELETE_MODAL
    }
}

export function hideAtmDeleteModal() {
    return {
        type: HIDE_ATM_DELETE_MODAL
    }
}

export function showAtmEditModal() {
    return {
        type: SHOW_ATM_EDIT_MODAL
    }
}

export function hideAtmEditModal() {
    return {
        type: HIDE_ATM_EDIT_MODAL
    }
}
