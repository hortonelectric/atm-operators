export const ADD_ATM_VALIDATE               = 'ADD_ATM_VALIDATE'
export const EDIT_ATM_VALIDATE 		 		= 'EDIT_ATM_VALIDATE'

export function onAddAtmError(data) {
    return {
        type: ADD_ATM_VALIDATE,
        data
    }
}

export function onEditAtmError(data) {
    return {
        type: EDIT_ATM_VALIDATE,
        data
    }
}
