import React, {Component} from 'react'
import { connect } from 'react-redux'

import AtmTable from './container/AtmTable'
import AtmDetail from './container/AtmDetail'
import AtmAdd from './container/AtmAdd'
import AtmEdit from './container/AtmEdit'
import AtmDelete from './container/AtmDelete'

import { listAtms } from './middleware/middleware'
import { listOperators } from '../Operator/middleware/middleware'

class Atm extends Component {

    componentWillMount = () => {
        this.props.dispatch(listAtms())
        this.props.dispatch(listOperators())
    }

    render() {
        return (
            <section id="department">
                <div className="col-md-5">
                    <AtmTable />
                </div>
                <div className="col-md-7">
					<AtmDetail />
                </div>
				<AtmAdd />
				<AtmEdit />
				<AtmDelete />
            </section>
        )
    }
}

export default connect()(Atm)
