import React,{Component} from 'react'
import Modal from 'react-bootstrap/lib/Modal'

import Detail from './Form/Detail'
import Location from './Form/Location'
import Operator from './Form/Operator'

export default class EditForm extends Component {

	
	SubmitButton() {
		if(this.props.loader){
			return	(
				<button type="submit" className="button btn-primary ml10" disabled> 
					<i className="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading
				</button>
			)
		}else{
			return (
				<button type="submit" className="button btn-primary ml10"> 
					<span className="">Submit</span>
				</button>
			)	
		}
	}

    render() {
        const {
			fields: { 
				type,
				supported,
				direction,
				fees,
				limits,
				details,
				lat,
				long,
				location,
				street,
				city,
				state,
				zip,
				country,
				phone,
				openHours,
				operator
			}
		} = this.props;


		const operatorForm = () => {
			const role = sessionStorage.getItem('role') 	

			if(!this.props.fields.operator.value && role == 'admin'){
				return (
						<div>
							<h2 className="mt40 pb10">ATM Operator</h2>
							<div className="bs-component mb20">
								<div className="alert alert-warning">
									<strong>Warning!</strong> This atm is from the old database, and no operator yet has been selected. If you select an operator here, you will override the existing operator and attached it to the operators from our database.
								</div>
							</div>
							<Operator
								operator={operator}
								operatorOptions={this.props.operatorOptions}
								ajaxErrors={this.props.ajaxErrors}
							/>
						</div>
			   )
			}	

			if(this.props.fields.operator.value && role == 'admin'){
				return (
						<div>
							<h2 className="mt40 pb10">ATM Operator</h2>
							<Operator
								operator={operator}
								operatorOptions={this.props.operatorOptions}
								ajaxErrors={this.props.ajaxErrors}
							/>
						</div>
			   )
			}	
		}

        return (
            <Modal show={this.props.modal} onHide={this.props.handleHide} bsSize="lg">
                <div className="admin-form">

                    <div className="panel">
                        <div className="panel-heading text-center">
                            <span className="panel-title">
                                <i className="fa fa-pencil" />Edit ATM
                            </span>
                        </div>
                    </div>

                    <form onSubmit={::this.props.handleSubmitForm}>

                        <div className="panel-body p25">

                            <h2 className="pb10">ATM Details</h2>
                            <Detail
                                type={type}
                                supported={supported}
                                direction={direction}
								fees={fees}
								limits={limits}
								details={details}
                                ajaxErrors={this.props.ajaxErrors}
                            />

                            <h2 className="mt40 pb10">ATM Location</h2>
                            <Location
                                lat={lat}
                                long={long}
								location={location}
                                street={street}
								city={city}
								state={state}
								zip={zip}
								country={country}
								phone={phone}
								openHours={openHours}
                                ajaxErrors={this.props.ajaxErrors}
                            />
							
							{operatorForm()}

                        </div>

                        <div className="panel-footer text-center">
                            <button type="button" className="button btn-default" onClick={this.props.handleHide}>Close</button>
							{ this.SubmitButton() }
                        </div>

                    </form>
                </div>
            </Modal>
        )

    }
}
