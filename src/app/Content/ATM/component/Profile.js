import React,{Component} from 'react'
import moment from 'moment'

export default class Profile extends Component {

    render() {

		const profile = this.props.profile

		const direction = (direction) => {
			if(direction == 0) return 'Fiat → Crypto'
			if(direction == 1) return 'Fiat ⇄ Crypto'
		}

		const operatorDetails = () => {
			if(profile.operator.id && this.props.operators[0]){
				const operator = this.props.operators.find( item => item._id == profile.operator.id )
				return(
					<div className="panel-body">
						<h4><small>OPERATOR : <strong>{ operator.company || 'N/A'}</strong></small></h4>
						<h4><small>PHONE : <strong>{ operator.details.phone || 'N/A'}</strong></small></h4>
						<h4><small>EMAIL : <strong>{ operator.details.email || 'N/A'}</strong></small></h4>
					</div>
				)
			}		
			if(!profile.operator.id){
				return(
					<div className="panel-body">
						<h4><small>OPERATOR : <strong>{ profile.operator.name || 'N/A'}</strong></small></h4>
						<h4><small>PHONE : <strong>{ profile.operator.phone || 'N/A'}</strong></small></h4>
						<h4><small>EMAIL : <strong>{ profile.operator.email || 'N/A'}</strong></small></h4>
					</div>
				)
			}		
		}

        return (
            <div className="panel panel-primary panel-border top">
                <div className="panel-heading" style={{height: '43px'}}>
                    <button className="btn btn-primary btn-xs ml10"
                            onClick={ this.props.handleAddAtm }
					>
                        <span className="fa fa-plus"></span>  Add ATM
                    </button>

                    <button className="btn btn-info btn-xs ml10"
                            onClick={ this.props.handleEditAtm }
					>
                        <span className="fa fa-pencil"></span>  Edit ATM
					</button>

                    <button className="btn btn-danger btn-xs ml10"
                            onClick={ this.props.handleDeleteAtm }
					>
                        <span className="fa fa-trash"></span>  Delete ATM
                    </button>

                </div>
                <div className="panel-body profile">
                    <div className="panel">

                        <div className="media clearfix">
                            <div className="media-body va-m">
                                <h2 className="media-heading">
									{ profile.atm.type }
                                </h2>
                                <h4>DIRECTION: { direction(profile.atm.direction)}</h4>
                                <h4>SUPPORTED: { profile.atm.supported.map( item => item + ' ' ) || 'N/A'}</h4>

                            </div>
                        </div>
						
                        <h3>Location Information</h3>
                        <div className="panel-body">
                            <h4><small>COORDINATES : <u>lat</u>: <strong>{ profile.location.lat || 'N/A' }</strong> <u>long</u>: <strong>{ profile.location.long || 'N/A' }</strong></small></h4>
                            <h4><small>LOCATION : <strong>{ profile.location.location || 'N/A' }</strong></small></h4>
                            <h4><small>STREET : <strong>{ profile.location.address.street || 'N/A'}</strong></small></h4>
                            <h4><small>CITY : <strong>{ profile.location.address.city || 'N/A' }</strong></small></h4>
                            <h4><small>STATE : <strong>{ profile.location.address.state || 'N/A'}</strong></small></h4>
                            <h4><small>ZIP : <strong>{ profile.location.address.zip || 'N/A'}</strong></small></h4>
                            <h4><small>COUNTRY : <strong>{ profile.location.address.country || 'N/A'}</strong></small></h4>
                            <h4><small>CONTACT NUMBER : <strong>{ profile.location.phone || 'N/A'}</strong></small></h4>
                            <h4><small>OPEN HOURS : <strong>{ profile.location.openHours || 'N/A'}</strong></small></h4>
                        </div>

                        <h3>Operator Information</h3>
						{operatorDetails()}

                        <h3>Other Information</h3>
                        <div className="panel-body">
                            <h4><small>FEES : <strong>{ profile.atm.fees || 'N/A'}</strong></small></h4>
                            <h4><small>LIMITS : <strong>{ profile.atm.limits || 'N/A'}</strong></small></h4>
                            <h4><small>DETAILS : <br/> <strong>{ profile.atm.details || 'N/A'} </strong></small></h4>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
