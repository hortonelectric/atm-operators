import React,{Component} from 'react'

export default class Table extends Component {
    render() {
		return (
			<div className="panel employee-table-panel">
				<div className="panel-heading">
					<span className="panel-title">
					<span className="fa fa-user" />Operators</span>
				</div>
				<div className="panel-body pn">
					<div className="bs-component">
						<Content rows={this.props.rows} loader={this.props.loader}/>
					</div>
				</div>
			</div>
		)
    }
}

class Content extends Component {
	render() {
	
		if(this.props.loader){
			return (
				<p className="text-center table-spinner">
					<i className="fa fa-spinner fa-spin" aria-hidden="true"></i>
					<br />
					<span style={{fontSize: 18}}>Loading</span>
				</p>                    
			)	
		}
	
		if(!this.props.loader){
			return (
				<table className="table table-hover">
					<thead>
					<tr>
						<th>Type</th>
						<th>Operator</th>
						<th>Location</th>
					</tr>
					</thead>
					<tbody>
						{this.props.rows}
					</tbody>
				</table>
			)	
		}

	}
}

// export default class Table extends Component {
//     render() {
//
//         return (
//             <div className="panel employee-table-panel">
//                 <div className="panel-heading">
//                     <span className="panel-title">
//                     <span className="fa fa-map-marker" />ATM Machines</span>
//                 </div>
//                 <div className="panel-body pn">
//                     <div className="bs-component">
//                         <table className="table table-hover">
//                             <thead>
//                             <tr>
//                                 <th>Type</th>
//                                 <th>Operator</th>
//                                 <th>Location</th>
//                             </tr>
//                             </thead>
//                             <tbody>
// 								{this.props.rows}
//                             </tbody>
//                         </table>
//                     </div>
//                 </div>
//             </div>
//         )
//     }
// }
