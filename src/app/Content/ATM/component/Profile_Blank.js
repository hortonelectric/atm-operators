import React,{Component} from 'react'

export default class Profile extends Component {

    render() {
        return (
            <div className="panel panel-primary panel-border top">
                <div className="panel-heading" style={{height: '43px'}}>
                    <button className="btn btn-primary btn-xs ml10"
                            onClick={ this.props.handleAddAtm }
					>
                        <span className="fa fa-plus"></span>  Add ATM
                    </button>

                    <button className="btn btn-info btn-xs ml10" disabled>
                        <span className="fa fa-pencil"></span>  Edit ATM
					</button>

                    <button className="btn btn-danger btn-xs ml10" disabled>
                        <span className="fa fa-trash"></span>  Delete ATM
                    </button>

                </div>
                <div className="panel-body profile">
                    <div className="panel">

                        <div className="media clearfix">
                            <div className="media-body va-m">
                                <h2 className="media-heading">
									Atm Name
                                </h2>
                                <h4>DIRECTION: </h4>
                                <h4>SUPPORTED: </h4>

                            </div>
                        </div>
						
                        <h3>Location Information</h3>
                        <div className="panel-body">
                            <h4><small>COORDINATES : </small></h4>
                            <h4><small>CITY : </small></h4>
                            <h4><small>LOCATION : </small></h4>
                            <h4><small>ADDRESS : </small></h4>
                            <h4><small>OPEN HOURS : </small></h4>
                        </div>

                        <h3>Operator Information</h3>
                        <div className="panel-body">
                            <h4><small>OPERATOR : </small></h4>
                            <h4><small>PHONE : </small></h4>
                            <h4><small>EMAIL : </small></h4>
                        </div>

                        <h3>Other Information</h3>
                        <div className="panel-body">
                            <h4><small>FEES : </small></h4>
                            <h4><small>LIMITS : </small></h4>
                            <h4><small>DETAILS :  </small></h4>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
