import React,{Component} from 'react'

export default class Detail extends Component {

    shouldComponentUpdate(nextProps) {
        return  this.props.type  	    !== nextProps.type  		||
				this.props.supported    !== nextProps.supported 	||
				this.props.direction    !== nextProps.direction   	||
				this.props.fees         !== nextProps.fees   		||
				this.props.limits       !== nextProps.limits   		||
				this.props.details      !== nextProps.details   	||
                this.props.ajaxErrors   !== nextProps.ajaxErrors;
    }

    errorDescription(input) {
        return this.props.ajaxErrors[input] ?
            this.props.ajaxErrors[input].map(function(error){
                return <em className="state-error" key="">{error}</em>
            }) : ''
    }

    render() {
        return (
            <div className="section">

                <div className="section row">
                    <div className="col-xs-12 col-md-6">
                        <label htmlFor="type"
						    className={`field prepend-icon ${this.props.ajaxErrors.type ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="type"
                                   id="type"
                                   className="gui-input"
                                   placeholder="ATM Type"
									{...this.props.type}

                            />
                            <label htmlFor="type" className="field-icon">
                                <i className="fa fa-building" />
                            </label>
                        </label>
                        { this.errorDescription('type') }

					</div>

                    <div className="col-xs-12 col-md-6">
						<label className={`field select ${this.props.ajaxErrors.direction ? 'state-error': ''}`}>
							<select id="direction" 
									name="direction" 
									{...this.props.direction}	
							>
								<option value="0">Fiat → Crypto</option>
								<option value="1">Fiat ⇄ Crypto</option>
							</select>
							<i className="arrow"></i>
						</label>
						{ this.errorDescription('direction') }
                    </div>
				</div>


                <div className="section row">
                    <div className="col-xs-12 col-md-6">
                        <label htmlFor="fees"
						    className={`field prepend-icon ${this.props.ajaxErrors.fees ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="fees"
                                   id="fees"
                                   className="gui-input"
                                   placeholder="ATM Fees"
									{...this.props.fees}

                            />
                            <label htmlFor="fees" className="field-icon">
                                <i className="fa fa-calculator" />
                            </label>
                        </label>
                        { this.errorDescription('fees') }
                    </div>
                    <div className="col-xs-12 col-md-6">
                        <label htmlFor="limits"
						    className={`field prepend-icon ${this.props.ajaxErrors.limits ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="limits"
                                   id="limits"
                                   className="gui-input"
                                   placeholder="ATM Limits"
									{...this.props.limits}

                            />
                            <label htmlFor="limits" className="field-icon">
                                <i className="fa fa-scissors" />
                            </label>
                        </label>
                        { this.errorDescription('limits') }
                    </div>
				</div>

                <div className="section row">
                    <div className="col-xs-12 col-md-6">
                        <label htmlFor="details"
						    className={`field prepend-icon ${this.props.ajaxErrors.details ? 'state-error': ''}`}
                        >
							<textarea className="gui-textarea" 
									   id="details" 
									   name="details" 
									   placeholder="Details here..."
									   style={{height:121}}
										{...this.props.details}	
							></textarea>
							<label htmlFor="details" className="field-icon">
								<i className="fa fa-comments"></i>
							</label>
						</label>
						<span className="input-footer">
							<strong>Hint:</strong>  Write desciptions and details of how to locate the ATM Machine
						</span>
                        { this.errorDescription('details') }
                    </div>

					<div className="col-xs-12 col-md-6">
						<label className={`field select ${this.props.ajaxErrors.supported ? 'state-error': ''}`}>
							<select id="supported" 
									name="supported" 
									style={{height:120}}
									multiple
									{...this.props.supported}	
							>
								<option key="Bitcoin" value="Bitcoin">Bitcoin</option>
								<option key="Ether" value="Ether">Ether</option>
								<option key="Litecoin" value="Litecoin">Litecoin</option>
								<option key="Darkcoin" value="Darkcoin">Darkcoin</option>
								<option key="Dogecoin" value="Dogecoin">Dogecoin</option>
							</select>
						</label>
						<span className="input-footer" style={{ bottom:-78 }}>
							<strong>Hint:</strong>  Hold <kbd>ctrl</kbd> and click on the cryptocurrencies you want to select
						</span>
					</div>

				</div>
            </div>
        )
    }
}
