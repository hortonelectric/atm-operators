import React,{Component} from 'react'
import countries from '../../../../../../countries.json'

export default class Location extends Component {

    shouldComponentUpdate(nextProps) {
        return  this.props.lat		 	!== nextProps.lat      		||
				this.props.long		 	!== nextProps.long		 	||
                this.props.location   	!== nextProps.location    	||
                this.props.street   	!== nextProps.street    	||
                this.props.city      	!== nextProps.city       	||
				this.props.state 		!== nextProps.state  	 	||
				this.props.zip 		 	!== nextProps.zip  	 		||
				this.props.country 		!== nextProps.country  	 	||
				this.props.phone		!== nextProps.phone			||
				this.props.openHours 	!== nextProps.openHours  	||
                this.props.ajaxErrors   !== nextProps.ajaxErrors;

    }

    errorDescription(input) {
        return this.props.ajaxErrors[input] ?
            this.props.ajaxErrors[input].map(function(error){
                return <em className="state-error" key="">{error}</em>
            }) : ''
    }

    render() {
		
		const countryListOptions = countries.map((country, index) => {
			return (
				<option key={index} value={country.name}>{country.name}</option>
			)	
		})

        return (
            <section>

                <div className="section row">
                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="lat"
						   className={`field prepend-icon ${this.props.ajaxErrors.lat ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="lat"
                                   id="lat"
                                   className="gui-input"
                                   placeholder="Latitude"
									{...this.props.lat}
                            />
                            <label htmlFor="lat" className="field-icon">
                                <i className="fa fa-map-marker" />
                            </label>
                        </label>
                        { this.errorDescription('lat') }
                    </div>
                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="long"
						   className={`field prepend-icon ${this.props.ajaxErrors.long ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="long"
                                   id="long"
                                   className="gui-input"
                                   placeholder="Longtitude"
									{...this.props.long}
                            />
                            <label htmlFor="long" className="field-icon">
                                <i className="fa fa-map-marker" />
                            </label>
                        </label>
                        { this.errorDescription('long') }
                    </div>
                </div>

                <div className="section row">
				
                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="phone"
						   className={`field prepend-icon ${this.props.ajaxErrors.phone ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="phone"
                                   id="phone"
                                   className="gui-input"
                                   placeholder="Phone of the location/establishment"
                                {...this.props.phone}
                            />
                            <label htmlFor="phone" className="field-icon">
                                <i className="fa fa-phone" />
                            </label>
                        </label>
                        { this.errorDescription('phone') }
                    </div>

                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="location"
						   className={`field prepend-icon ${this.props.ajaxErrors.location ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="location"
                                   id="location"
                                   className="gui-input"
                                   placeholder="Location of the ATM"
                                {...this.props.location}
                            />
                            <label htmlFor="location" className="field-icon">
                                <i className="fa fa-building" />
                            </label>
                        </label>
                        { this.errorDescription('location') }
                    </div>

                </div>

                <div className="section row">

                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="street"
						   className={`field prepend-icon ${this.props.ajaxErrors.street ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="street"
                                   id="street"
                                   className="gui-input"
                                   placeholder="Street"
                                {...this.props.street}
                            />
                            <label htmlFor="street" className="field-icon">
                                <i className="fa fa-" />
                            </label>
                        </label>
                        { this.errorDescription('street') }
                    </div>

                    <div className="col-md-6 col-xs-12">
                        <label htmlFor="city"
						   className={`field prepend-icon ${this.props.ajaxErrors.city ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="city"
                                   id="city"
                                   className="gui-input"
                                   placeholder="City"
                                {...this.props.city}
                            />
                            <label htmlFor="city" className="field-icon">
                                <i className="fa fa-map" />
                            </label>
                        </label>
                        { this.errorDescription('city') }
                    </div>

                </div>

                <div className="section row">

                    <div className="col-md-4 col-xs-12">
                        <label htmlFor="state"
						   className={`field prepend-icon ${this.props.ajaxErrors.state ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="state"
                                   id="state"
                                   className="gui-input"
                                   placeholder="State (If USA, please put state code ie:NY)"
                                {...this.props.state}
                            />
                            <label htmlFor="state" className="field-icon">
                                <i className="fa fa-" />
                            </label>
                        </label>
                        { this.errorDescription('state') }
                    </div>

                    <div className="col-md-4 col-xs-12">
                        <label htmlFor="zip"
						   className={`field prepend-icon ${this.props.ajaxErrors.zip ? 'state-error': ''}`}
                        >
                            <input type="text"
                                   name="zip"
                                   id="zip"
                                   className="gui-input"
                                   placeholder="Zip Code"
                                {...this.props.zip}
                            />
                            <label htmlFor="zip" className="field-icon">
                                <i className="fa fa-" />
                            </label>
                        </label>
                        { this.errorDescription('zip') }
                    </div>

                    <div className="col-md-4 col-xs-12">
						<label className={`field select ${this.props.ajaxErrors.country ? 'state-error': ''}`}>
							<select name="country"
                                    id="country"
									{...this.props.country}	
							>
								{ countryListOptions }
							</select>
							<i className="arrow"></i>
                        </label>
                        { this.errorDescription('country') }
                    </div>

                </div>

				<div className="row">
				
                    <div className="col-xs-12">
                        <label htmlFor="openHours"
						   className={`field prepend-icon ${this.props.ajaxErrors.openHours ? 'openHours-error': ''}`}
                        >
                            <input type="text"
                                   name="openHours"
                                   id="openHours"
                                   className="gui-input"
                                   placeholder="Open Hours"
                                {...this.props.openHours}
                            />
                            <label htmlFor="openHours" className="field-icon">
                                <i className="fa fa-clock-o" />
                            </label>
                        </label>
                        { this.errorDescription('openHours') }
                    </div>
				</div>

            </section>
        )
    }
}
