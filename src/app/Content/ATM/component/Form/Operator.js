import React,{Component} from 'react'

export default class Location extends Component {

    shouldComponentUpdate(nextProps) {
        return  this.props.operator			!== nextProps.operator      	||
				this.props.operatorOptions 	!== nextProps.operatorOptions 	||
                this.props.ajaxErrors   	!== nextProps.ajaxErrors;

    }

    errorDescription(input) {
        return this.props.ajaxErrors[input] ?
            this.props.ajaxErrors[input].map(function(error){
                return <em className="state-error" key="">{error}</em>
            }) : ''
    }

    render() {
        return (
            <section>

                <div className="section row">
                    <div className="col-xs-12">
						<label className="field select">
							<select id="operator" 
									name="operator" 
									{...this.props.operator}	
							>
								{ this.props.operatorOptions }
							</select>
							<i className="arrow"></i>
						</label>
                        { this.errorDescription('operator') }
                    </div>
                </div>

            </section>
        )
    }
}
