import * as ACTION from '../action/action'

export const atms = (state = [], action) => {

    switch (action.type) {

        case ACTION.LIST_ATMS :
			return [ ...action.data ]

        case ACTION.ADD_ATM :
			return [ ...state, action.data ]

        case ACTION.EDIT_ATM :
            return state.map(atm => atm._id === action.data._id ? action.data : atm)

        case ACTION.DELETE_ATM : 
            return state.filter(atm => atm._id !== action.data._id)

        default:
            return state
    }
}

export const selected = (state = null, action) => {

    switch (action.type) {
        case ACTION.SELECT_ATM :
            return action.id
        case ACTION.DELETE_ATM :
            return null
        default :
            return state
    }

}
