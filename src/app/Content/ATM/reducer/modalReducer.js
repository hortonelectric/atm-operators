import * as ACTION from '../action/modalAction'

export const atmAddModal = (state = false, action) => {
    switch (action.type) {
        case ACTION.SHOW_ATM_ADD_MODAL :
            return true
        case ACTION.HIDE_ATM_ADD_MODAL :
            return false
        default :
            return state
    }
}

export const atmDeleteModal = (state = false, action) => {
    switch (action.type) {
        case ACTION.SHOW_ATM_DELETE_MODAL :
            return true
        case ACTION.HIDE_ATM_DELETE_MODAL :
            return false
        default :
            return state
    }
}

export const atmEditModal = (state = false, action) => {
    switch (action.type) {
        case ACTION.SHOW_ATM_EDIT_MODAL :
            return true
        case ACTION.HIDE_ATM_EDIT_MODAL :
            return false
        default :
            return state
    }
}
