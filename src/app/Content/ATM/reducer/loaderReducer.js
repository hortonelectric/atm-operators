import * as ACTION_LOADER from '../action/loaderAction'
import * as ACTION from '../action/action'

export const tableLoader = (state = false, action) => {
    switch (action.type) {
        case ACTION_LOADER.ATM_TABLE_LOADER_ON :
            return true
        case ACTION_LOADER.ATM_TABLE_LOADER_OFF :
            return false
        default :
            return state
    }
}

export const addFormLoader = (state = false, action) => {
    switch (action.type) {
        case ACTION_LOADER.ATM_ADD_FORM_LOADER_ON :
            return true
        case ACTION_LOADER.ATM_ADD_FORM_LOADER_OFF :
            return false
        default :
            return state
    }
}


export const editFormLoader = (state = false, action) => {
    switch (action.type) {
        case ACTION_LOADER.ATM_EDIT_FORM_LOADER_ON :
            return true
        case ACTION_LOADER.ATM_EDIT_FORM_LOADER_OFF :
            return false
        default :
            return state
    }
}

export const deleteFormLoader = (state = false, action) => {
    switch (action.type) {
        case ACTION_LOADER.ATM_DELETE_FORM_LOADER_ON :
            return true
        case ACTION_LOADER.ATM_DELETE_FORM_LOADER_OFF :
            return false
        default :
            return state
    }
}
