import * as ERROR_ACTION from '../action/errorAction'
import { ADD_ATM, SELECT_ATM, EDIT_ATM } from '../action/action'

export const addAtmError = (state = {}, action) => {
    switch (action.type) {
        case ERROR_ACTION.ADD_ATM_VALIDATE :
            return action.data
        case ADD_ATM :
            return {}
        default :
            return state
    }
}

export const editAtmError = (state = {}, action) => {
    switch (action.type) {
        case ERROR_ACTION.EDIT_ATM_VALIDATE :
            return action.data
        case SELECT_ATM :
            return {}
        case EDIT_ATM :
            return {}
        default :
            return state
    }
}
