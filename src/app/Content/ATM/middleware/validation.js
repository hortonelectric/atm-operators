import Joi from 'joi-browser'

export const validateAddAtm = (data, cb) => {

    let Schema = Joi.object({
        atm : Joi.object().keys({
            type 		: Joi.string().required(),
			direction	: Joi.boolean().required(),
			supported	: Joi.array().min(1).required(),
            fees 		: Joi.any().optional(),
            limits 		: Joi.any().optional(),
            details 	: Joi.any().optional()
        }),
        location : Joi.object().keys({
			lat			: Joi.number().required(),
			long 		: Joi.number().required(),
            location	: Joi.string().required(),
            address     : Joi.object().keys({
				street		: Joi.string().required(),
				city		: Joi.any().required(),
				state		: Joi.any().optional(),
				zip			: Joi.any().optional(),
				country		: Joi.string().required()
			}),
			phone		: Joi.any().optional(),
			openHours	: Joi.any().optional()
        }),
		operator : Joi.object().keys({
			id 			: Joi.string().required()	
		})
    })

    Joi.validate(data, Schema, { abortEarly: false }, (err, value) => {
        if(err) cb(err)
        if(!err) cb(null)
    })

}

export const validateEditAtm = (data, cb) => {

    let Schema = Joi.object({
        atm : Joi.object().keys({
            type 		: Joi.string().required(),
			direction	: Joi.boolean().required(),
			supported	: Joi.array().min(1).required(),
            fees 		: Joi.any().optional(),
            limits 		: Joi.any().optional(),
            details 	: Joi.any().optional()
        }),
        location : Joi.object().keys({
			lat			: Joi.number().required(),
			long 		: Joi.number().required(),
            location	: Joi.string().required(),
            address     : Joi.object().keys({
				street		: Joi.string().required(),
				city		: Joi.any().required(),
				state		: Joi.any().optional(),
				zip			: Joi.any().optional(),
				country		: Joi.string().required()
			}),
			phone		: Joi.any().optional(),
			openHours	: Joi.any().optional()
        }),
		operator : Joi.object().keys({
			id 			: Joi.any().optional()	
		})
    })

    Joi.validate(data, Schema, { abortEarly: false }, (err, value) => {
        if(err) cb(err)
        if(!err) cb(null)
    })

}
