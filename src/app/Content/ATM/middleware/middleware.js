import async from 'async'

import * as ajax from './ajax'
import * as validate from './validation'
import * as modal from '../action/modalAction'
import * as action from '../action/action'
import * as errorAction from '../action/errorAction'
import * as loaderAction from '../action/loaderAction'
import { handle } from './handleResponse'
import {logout} from '../../../User/middleware/middleware'

export const listAtms = () => {

    return dispatch => {

		dispatch(loaderAction.tableLoaderOn())
        ajax.listAtms((status, response) => {
            if(status === 401) dispatch(handleUnauthorized())
            if(status === 200) dispatch(action.listAtms(response.data))
			dispatch(loaderAction.tableLoaderOff())
        })

    }
}

export const addAtm = (data) => {


	function validateOnBrowser(payload, cb) {
		validate.validateAddAtm(payload, err => {
			if(err) cb(err, null)
			if(!err) cb(null,payload)
		})
	}

	function ajaxCreateAtm(data, cb) {
		ajax.addAtm(data, (status, response) => {
			if(status === 401) cb(status, null)
			if(response.error) cb(response.error, null)
			if(!response.error)cb(null, response)
		})
	}

    return dispatch =>{
		dispatch(loaderAction.addFormLoaderOn())
        async.waterfall([
            async.apply(validateOnBrowser, data),
            ajaxCreateAtm
        ], (err, result) => {
            if(err && err === 401) dispatch(handleUnauthorized())
            if(err && err !== 401) dispatch(handle(err,result, errorAction.onAddAtmError, null))
            if(!err) {
                const option = {
                    modal : modal.hideAtmAddModal,
                    message : `You have successfully added ${result.atm.type} on ${result.location.address.city} `,
                    action : action.addAtm
                }
                dispatch(handle(err,result, action.addAtm, option))
            }
			dispatch(loaderAction.addFormLoaderOff())
        })
    }
}

export const editAtm = (atm, payload) => {

    function validateOnBrowser(payload, cb) {
        validate.validateEditAtm(payload, err => {
            if(err) cb(err, null)
            if(!err) cb(null,payload)
        })
    }

    function ajaxToAPI(data, cb) {
        ajax.editAtm(atm._id ,payload, (status,response) => {
            if(status === 401) cb(status, null)
            if(response.error) cb(response.error, null)
            if(!response.error)cb(null, response)
        })
    }

    return dispatch =>{
		dispatch(loaderAction.editFormLoaderOn())
        async.waterfall([
            async.apply(validateOnBrowser, payload),
            ajaxToAPI
        ], (err, result) => {
            if(err && err === 401) dispatch(handleUnauthorized())
            if(err && err !== 401) dispatch(handle(err,result, errorAction.onEditAtmError, null))
            if(!err) {
                const option = {
                    modal : modal.hideAtmEditModal,
                    message : `You have successfully updated ${result.atm.type} that is on ${result.location.location}`
                }
                dispatch(handle(err,result, action.editAtm, option))
            }
			dispatch(loaderAction.editFormLoaderOff())
        })
    }

}

export const deleteAtm = ( atm ) => {

    return dispatch => {
		dispatch(loaderAction.deleteFormLoaderOn())
        ajax.deleteAtm(atm._id, (status, response)=> {
            if(status === 401) dispatch(handleUnauthorized())
            if(response.error && status !== 401) dispatch(handle(err,result, null, null))
            if(!response.error && status !== 401) {
                const option = {
                    modal : modal.hideAtmDeleteModal,
                    message : `You have successfully deleted ${atm.atm.type} in ${atm.location.location} from ${atm.location.address.city} city`,
                }
                dispatch(handle(null,atm, action.deleteAtm, option))
            }
			dispatch(loaderAction.deleteFormLoaderOff())
        })
    }

}

const handleUnauthorized = () => {
    return dispatch => {
        dispatch(logout({message: 'Your token is invalid or expired, you will be redirected to the log-in page'}))
    }
}




// export const uploadPicture = ( employee ) => {
//
//     const tabs = [
//         'file',
//         'camera',
//         'url',
//         'facebook'
//     ]
//
//     const options = {
//         imagesOnly : true,
//         previewStep: true,
//         crop : '1x1',
//         tabs  : tabs
//     }
//
//     function uploadPicture (employee, cb) {
//         uploadcare.openDialog(null, options)
//             .done( file => {
//                 file.done(result => {
//                         if(result.uuid) return cb(null, result, employee)
//                     })
//                     .fail( err => {
//                         return cb(err, null)
//                     })
//             })
//             .fail (err => {
//                 return cb(err, null)
//             })
//     }
//
//     function ajaxToAPI (result, employee, cb) {
//         ajax.editPicture(employee.id, {cdn : `https://ucarecdn.com/${result.uuid}/`}, (status, response) =>{
//             if(status === 401) cb(status, null)
//             if(response.error) cb(response.error, null)
//             if(!response.error)cb(null, response, employee)
//         })
//     }
//
//     return dispatch => {
//         async.waterfall([
//             async.apply(uploadPicture, employee),
//             ajaxToAPI
//         ], (err, result, employee) => {
//             if(err && err === 401) dispatch(handleUnauthorized())
//             if(err && err !== 401) dispatch(handle(err,result, null, null))
//             if(!err) {
//                 const option = {
//                     message :
//                         `You have successfully updated
//                         ${employee.gender ? 'Ms. ' : 'Mr. ' }
//                         ${employee.firstname}
//                         ${employee.lastname}
//                         picture`
//                 }
//                 dispatch(handle(err,result, action.uploadPicture, option))
//             }
//         })
//     }
//
// }

