import request from 'superagent'
import config from '../../../../../config.json'

const api = config.current

export const listAtms = (cb) => {
	const authorization = sessionStorage.getItem('header')
	request
        .get(`${api}atm`)
        .set('Authorization', authorization)
        .end((error, response) => {
			cb(response.status, response.body)
        })
}

export const addAtm = (data, cb) => {
	const authorization = sessionStorage.getItem('header')
	request
        .post(`${api}atm`)
        .set('Authorization', authorization)
        .send(data)
        .end((error, response) => {
            cb(response.status, response.body)
        })
}


export const editAtm = (id, data, cb) => {
	const authorization = sessionStorage.getItem('header')
	request
        .put(`${api}atm/${id}`)
        .set('Authorization', authorization)
        .send(data)
        .end((error, response) => {
            cb(response.status, response.body)
        })
}

export const  deleteAtm = (id, cb) => {
	const authorization = sessionStorage.getItem('header')
    request
        .del(`${api}atm/${id}`)
        .set('Authorization', authorization)
        .end((error, response) => {
            cb(response.status, response.body)
        })
}

export const editPicture = (id, data, cb) => {
    request
        .put(`${api}employees/${id}/photo`)
        .send(data)
        .end((error, response) => {
            if(response.status === 500) {
                addPicture(id, data, cb)
            }else{
                cb(response.status, response.body)
            }
        })
}

const addPicture = (id, data, cb) => {
    request
        .post(`${api}employees/${id}/photo`)
        .send(data)
        .end((error, response) => {
            cb(response.status, response.body)
        })
}
