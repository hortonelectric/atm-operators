import React from 'react'
import {connect} from 'react-redux'

class Header extends React.Component {

    render() {
        return (
            <header className="navbar navbar-fixed-top bg-dark">
                <div className="navbar-branding">
                    <a className="navbar-brand" href="dashboard.html">
                        <b>ATM Bitcoin </b>Opreators
                    </a>
                    <span id="toggle_sidemenu_l" className="ad ad-lines" />
                </div>
                <ul className="nav navbar-nav navbar-right">
                    <li className="menu-divider hidden-xs">
                        <i className="fa fa-circle" />
                    </li>
                    <li className="dropdown">
                        <a href="#" className="dropdown-toggle fw600 p15" data-toggle="dropdown">
                            <img src="assets/img/avatars/1.jpg" alt="avatar" className="mw30 br64 mr15" />
							{this.props.profile.name.first} {this.props.profile.name.last}
                            <span className="caret caret-tp hidden-xs" />
                        </a>
                        <ul className="dropdown-menu list-group dropdown-persist w250" role="menu">
                            <li className="dropdown-header clearfix">
                                <div className="pull-left ml10">
                                    <select id="user-status">
                                        <optgroup label="Current Status:">
                                            <option value="1-1">Away</option>
                                            <option value="1-2">Offline</option>
                                            <option value="1-3">Online</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div className="pull-right mr10">
                                    <select id="user-role">
                                        <optgroup label="Logged in As:">
                                            <option value="1-1">Client</option>
                                            <option value="1-2">Editor</option>
                                            <option value="1-3">Admin</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </li>
                            <li className="list-group-item">
                                <a href="#" className="animated animated-short fadeInUp">
                                    <span className="fa fa-envelope" /> Messages
                                    <span className="label label-warning">2</span>
                                </a>
                            </li>
                            <li className="list-group-item">
                                <a href="#" className="animated animated-short fadeInUp">
                                    <span className="fa fa-user" /> Friends
                                    <span className="label label-warning">6</span>
                                </a>
                            </li>
                            <li className="list-group-item">
                                <a href="#" className="animated animated-short fadeInUp">
                                    <span className="fa fa-gear" /> Account Settings </a>
                            </li>
                            <li className="list-group-item">
                                <a href="#" className="animated animated-short fadeInUp">
                                    <span className="fa fa-bell" /> Activity</a>
                            </li>
                            <li className="dropdown-footer">
                                <a href="#" className>
                                    <span className="fa fa-power-off pr5" /> Logout </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </header>
        )
    }
}

export default connect(
	state => ({
			profile : state.user.profile
		})
)(Header)
