import React from 'react'
import { render } from 'react-dom'
import thunk from 'redux-thunk'
import reducer from './combinedReducers'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, IndexRedirect, Route, browserHistory } from 'react-router'

import { UserAuthWrapper } from 'redux-auth-wrapper'
import { authenticate } from './app/User/middleware/middleware'

import Container from './app/Container'
import Operator from './app/Content/Operator/Operator'
import ATM from './app/Content/ATM/ATM'
import Login from './app/User/Login'

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const store = createStoreWithMiddleware(reducer)
const rootElement = document.getElementById('app')

const UserIsAuthenticated = UserAuthWrapper({
    authSelector: state => state.user.profile,
    redirectAction: authenticate,
    wrapperDisplayName: 'UserIsAuthenticated'
})

render((
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={UserIsAuthenticated(Container)}>
                <IndexRedirect to="/operator" />
                <Route path="/operator" component={Operator}/>
                <Route path="/ATM" component={ATM}/>
            </Route>
			<Route path="/login" component={Login}/>
        </Router>
    </Provider>
), rootElement)
